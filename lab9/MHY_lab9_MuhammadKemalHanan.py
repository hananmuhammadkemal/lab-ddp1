from datetime import datetime

'''
Class: Karyawan
Attributes: username, password, nama, umur, role
Methods: getter, login, logout
'''
class Karyawan:
    def __init__(self, username, password, nama, umur, role):
        self.__username = username
        self.__password = password
        self.__nama = nama
        self.__umur = umur
        self.__role = role

    def get_username(self):
        return self.__username

    def get_password(self):
        return self.__password

    def get_nama(self):
        return self.__nama

    def get_umur(self):
        return self.__umur

    def get_role(self):
        return self.__role

    cash = 0
    kue = 0
    deterjen = 0
    pekerja = 0
    daftarkaryawan = []
    terakhir_dibersihkan = "None"
    

'''
Class: Kasir
Superclass: Karyawan
Attributes: username, password, nama, umur, role
Methods: terima_pembayaran
'''
class Kasir(Karyawan):
    def __init__(self, username, password, nama, umur, role):
        super().__init__(username, password, nama, umur, role)
        self.cash = 0
        self.__lastlogin = "None"

    def terima_pembayaran(self,nominal):
        print("Berhasil menerima uang sebanyak {}".format(nominal))
        Karyawan.cash += nominal
        self.cash += nominal

'''
Class: Janitor
Superclass: Karyawan
Attributes: username, password, nama, umur, role
Methods: bersihkan_toko, beli_deterjen
'''
        
class Janitor(Karyawan):
    def __init__(self, username, password, nama, umur, role):
        super().__init__(username, password, nama, umur, role)
        self.__bersih = 0
        self.__beli = 0
        self.__lastlogin = "None"

    def bersihkan_toko(self):
        if Karyawan.deterjen > 0:
            print("Toko telah dibersihkan")
            Karyawan.deterjen -= 1
            self.__bersih += 1
        else:
            print("Deterjen tidak cukup")

    def beli_deterjen(self, jumlah):
        Karyawan.deterjen += jumlah
        self.__beli += jumlah
        print("Berhasil membeli {} deterjen".format(jumlah))

'''
Class: Chef
Superclass: Karyawan
Attributes: username, password, nama, umur, role
Methods: buat_kue, buang_kue
'''

class Chef(Karyawan):
    def __init__(self, username, password, nama, umur, role):
        super().__init__(username, password, nama, umur, role)
        self.__buat = 0
        self.__buang = 0
        self.__lastlogin = "None"

    def buat_kue(self, jumlah):
        Karyawan.kue += jumlah
        self.__buat += jumlah
        print("Berhasil membuat {} kue".format(jumlah))

    def buang_kue(self, jumlah):
        if Karyawan.kue < jumlah:
            print("Tidak bisa membuang kue")
        else:
            Karyawan.kue -= jumlah
            self.__buang += jumlah

'''Menu'''
print("Selamat datang di Sistem Manajemen Homura")
while True:
    print()
    print("Apa yang ingin anda lakukan? (Tulis angka saja)")
    print("1. Register karyawan baru")
    print("2. Login")
    print("8. Status Report")
    print("9. Karyawan Report")
    print("11. Exit\n")

    try:
        pilihan = int(input("Pilihan: "))
    except ValueError:
        print("Input tidak valid")

    if pilihan == 1:
        print("Format data: [username] [password] [nama] [umur] [role]")
        data = input("Data: ").split()
        found = False
        if Karyawan.pekerja != []:
            for i in Karyawan.daftarkaryawan:
                if i.get_username() == data[0]:
                    print("Username tersebut telah digunakan. Silahkan coba lagi")
                    print()
                    found = True
        if found == False:
            if data[4].lower() == "kasir":
                Karyawan.daftarkaryawan.append(Kasir(data[0], data[1], data[2], data[3], data[4]))
            elif data[4].lower() == "janitor":
                Karyawan.daftarkaryawan.append(Janitor(data[0], data[1], data[2], data[3], data[4]))
            elif data[4].lower() == "chef":
                Karyawan.daftarkaryawan.append(Chef(data[0], data[1], data[2], data[3], data[4]))
            print("Berhasil register karyawan baru")
            Karyawan.pekerja += 1
            break

    elif pilihan == 2:
        username = input("Username: ")
        password = input("Password: ")
        try:
            user = Karyawan.daftarkaryawan[Karyawan.daftarkaryawan.index(username)]
            print("Selamat datang, {}".format(user.get_nama()))
            user.__lastlogin = datetime.now()
            if password != user.get_password():
                print("Password salah")
            else:
                if user.get_role().lower() == "kasir":
                    while True:
                        print()
                        print("Apa yang ingin anda lakukan? (Tulis angka saja)")
                        print("3. Terima pembayaran")
                        print("10. Logout")
                        print()
                        pilihan = int(input("Pilihan: "))
                        if pilihan == 3:
                            nominal = int(input("Jumlah pembayaran: "))
                            user.terima_pembayaran(nominal)
                        elif pilihan == 10:
                            print("LOGOUT BERHASIL")
                            break
                        elif pilihan == 1 or pilihan == 2 or pilihan == 8 or pilihan == 9 or pilihan == 11:
                            print("Anda harus logout terlebih dahulu untuk menjalankan fungsi ini")
                        elif pilihan == 4 or pilihan == 5:
                            print("Anda harus login sebagai janitor untuk menjalankan fungsi ini")
                        elif pilihan == 6 or pilihan == 7:
                            print("Anda harus login sebagai chef untuk menjalankan fungsi ini")
                        else:
                            print("Pilihan tidak valid")
                elif user.role == "janitor":
                    while True:
                        print()
                        print("Apa yang ingin anda lakukan? (Tulis angka saja)")
                        print("4. Bersihkan toko")
                        print("5. Beli deterjen")
                        print("10. Logout")
                        print()
                        pilihan = int(input("Pilihan: "))
                        if pilihan == 4:
                            user.bersihkan_toko()
                            Karyawan.terakhir_dibersihkan = str(datetime.now())
                        elif pilihan == 5:
                            jumlah = int(input("Jumlah deterjen yang ingin dibeli: "))
                            user.beli_deterjen(jumlah)
                        elif pilihan == 10:
                            print("LOGOUT BERHASIL")
                            break
                        elif pilihan == 1 or pilihan == 2 or pilihan == 8 or pilihan == 9 or pilihan == 11:
                            print("Anda harus logout terlebih dahulu untuk menjalankan fungsi ini")
                        elif pilihan == 3:
                            print("Anda harus login sebagai kasir untuk menjalankan fungsi ini")
                        elif pilihan == 6 or pilihan == 7:
                            print("Anda harus login sebagai chef untuk menjalankan fungsi ini")
                        else:
                            print("Pilihan tidak valid")
                elif user.role == "chef":
                    while True:
                        print()
                        print("Apa yang ingin anda lakukan? (Tulis angka saja)")
                        print("6. Buat kue")
                        print("7. Buang kue")
                        print("10. Logout")
                        print()
                        pilihan = int(input("Pilihan: "))
                        if pilihan == 6:
                            jumlah = int(input("Jumlah kue yang ingin dibuat: "))
                            user.buat_kue(jumlah)
                        elif pilihan == 7:
                            jumlah = int(input("Jumlah kue yang ingin dibuang: "))
                            user.buang_kue(jumlah)
                        elif pilihan == 10:
                            print("LOGOUT BERHASIL")
                            break
                        elif pilihan == 1 or pilihan == 2 or pilihan == 8 or pilihan == 9 or pilihan == 11:
                            print("Anda harus logout terlebih dahulu untuk menjalankan fungsi ini")
                        elif pilihan == 3:
                            print("Anda harus login sebagai kasir untuk menjalankan fungsi ini")
                        elif pilihan == 4 or pilihan == 5:
                            print("Anda harus login sebagai janitor untuk menjalankan fungsi ini")
                        else:
                            print("Pilihan tidak valid")
        except:
            print("Username/Password salah. Silahkan coba lagi")

    elif pilihan == 8:
        print(f'''
        ==============================
        STATUS TOKO HOMURA SAAT INI 
        Jumlah karyawan = {Karyawan.pekerja}
        Jumlah cash = {Karyawan.cash}
        Jumlah kue = {Karyawan.kue}
        Jumlah deterjen = {Karyawan.deterjen}
        Terakhir dibersihkan = {Karyawan.terakhir_dibersihkan}
        ==============================
        ''')

    elif pilihan == 9:
        if Karyawan.pekerja <= 0:
            print("Tidak ada karyawan")
        else:
            for i in Karyawan.daftarkaryawan:
                print(f'''
                ==============================
                Username = {i.get_username()}
                Nama = {i.get_nama()}
                Role = {i.get_role()}
                Last login = {i.__lastlogin}
                ''')
                if i.get_role().lower() == "kasir":
                    print(f"Jumlah cash = {globals()[i.get_username()].cash}")
                elif i.get_role().lower() == "janitor":
                    print(f"Jumlah deterjen = {globals()[i.get_username()].deterjen}")
                elif i.get_role().lower() == "chef":
                    print(f"Jumlah kue = {globals()[i.get_username()].kue}")
                print("=============================")

    elif pilihan == 11:
        print("TERIMA KASIH TELAH MENGGUNAKAN SISTEM MANAJEMEN HOMURA")

    elif pilihan == 3 or pilihan == 4 or pilihan == 5 or pilihan == 6 or pilihan == 7 or pilihan == 10:
        print("Anda harus login untuk menjalankan fungsi ini")
    
    else:
        print("Pilihan tidak valid")







