# Pembuka
print('''
===================================
  WELCOME TO KARTI-NIAN CHALLENGE
===================================
''')

# Masukkan jumlah acara yang akan dilombakan
try:
    jumlah_acara = int(input("Masukka jumlah acara yang akan dilombakan: "))
except ValueError:
    print()
    print("Masukkan angka saja")
    pass

# Masukkan acara yang ingin dilombakan
dict_lomba = {}
count = 1
while count < jumlah_acara+1:
    print()
    print(f"LOMBA {count}")
    nama_lomba = input("Nama lomba: ")
    try:
        hadiah_juara1 = int(input("Hadiah juara 1: "))
        hadiah_juara2 = int(input("Hadiah juara 2: "))
        hadiah_juara3 = int(input("Hadiah juara 3: "))
    except ValueError:
        print("Masukkan angka saja")
        pass
    count += 1
    dict_lomba[nama_lomba] = [hadiah_juara1, hadiah_juara2, hadiah_juara3]


# Data lomba dan hadiah
print('''
==================================
    DATA MATA LOMBA DAN HADIAH
==================================
''')

# Cetak data mata lomba dan hadiah
for nama_lomba, hadiah_lomba in sorted(dict_lomba.items(), key=lambda x: x[0]):
    print()
    print(f"Lomba {nama_lomba}")
    print(f"[Juara 1] {hadiah_lomba[0]}")
    print(f"[Juara 2] {hadiah_lomba[1]}")
    print(f"[Juara 3] {hadiah_lomba[2]}")

# Data Peserta
print('''
==================================
      DATA PESERTA CHALLENGE
==================================
''')

# Masukkan jumlah peserta
try:
    jumlah_peserta = int(input("Masukkan jumlah peserta: "))
    print()
except ValueError:
    print()
    print("Masukkan angka saja")
    pass

# Masukkan nama peserta
list_peserta = []
count = 1
while count < jumlah_peserta+1:
    nama_peserta = input(f"Nama peserta {count}: ")
    list_peserta.append(nama_peserta)
    count += 1

# Hilangkan nama peserta yang sama
set_peserta = set(map(str.lower, list_peserta))

# Challenge
print('''
==================================
        CHALLENGE DIMULAI
==================================
''')

# Masukkan pemenang - pemenang lomba
dict_juara = {}
for nama_lomba, hadiah_lomba in sorted(dict_lomba.items(), key=lambda x: x[0]):
    print(f"Lomba {nama_lomba}")
    juara1 = input("Juara 1: ")
    juara2 = input("Juara 2: ")
    juara3 = input("Juara 3: ")
    print()
    # Cek apakah pemenang ada di list peserta dan hitung total hadiah yang didapatkan
    if juara1.lower() in set_peserta:
        total_hadiah1 = 0
        total_hadiah1 += hadiah_lomba[0]
    if juara2.lower() in set_peserta:
        total_hadiah2 = 0
        total_hadiah2 += hadiah_lomba[1]
    if juara3.lower() in set_peserta:
        total_hadiah3 = 0
        total_hadiah3 += hadiah_lomba[2]
    dict_juara[nama_peserta.lower()][juara1.lower()] = [total_hadiah1, total_hadiah2, total_hadiah3]
    dict_juara[nama_peserta.lower()][juara2.lower()] = [total_hadiah1, total_hadiah2, total_hadiah3]
    dict_juara[nama_peserta.lower()][juara3.lower()] = [total_hadiah1, total_hadiah2, total_hadiah3]

# Hasil
print('''
==================================
           FINAL RESULT
==================================
''')

total_hadiah = total_hadiah1 + total_hadiah2 + total_hadiah3
for nama_peserta, juara in sorted(dict_juara.items(), key=lambda x: x[0]):
    print(f"{nama_peserta}")
    print(f"Total hadiah: {total_hadiah}")
    print(f"Juara yang diterima: {juara}")
    print()