harga_pulpen = 5000
harga_pensil = 2000
harga_cortape = 3000

print("Toko ini menjual:")
print("1. Pulpen " + str(harga_pulpen) + "/pcs")
print("2. Pensil " + str(harga_pensil) + "/pcs")
print("3. Correction Tape " + str(harga_cortape) + "/pcs")
print()

print("Masukkan jumlah barang yang ingin anda beli")
jml_pulpen = int(input("Pulpen: "))
jml_pensil = int(input("Pensil: "))
jml_cortape = int(input("Correction Tape: "))
print()

total = (jml_pulpen * harga_pulpen) + (jml_pensil * harga_pensil) + (jml_cortape * harga_cortape)

print("Ringkasan Pembelian")
print(f"{jml_pulpen} pulpen x {harga_pulpen}")
print(f"{jml_pensil} pensil x {harga_pensil}")
print(f"{jml_cortape} correction tape x {harga_cortape}")
print(f"Total harga: {total}")