# Masukkan nama file
file1 = input("Masukkan nama file input: ")
file2 = input("Masukkan nama file output: ")

try:
    count_total_lines = len(open(file1).readlines()) # Menghitung jumlah baris dalam file input
    file_output = open(file2, "w")
    read_file = open(file1).read()
    saved = 0 # Variabel untuk menghitung jumlah baris yang disimpan
    temp = 0
    new_string = ""
    for i in range(len(read_file)): 
        if read_file[i].isdigit(): # Jika karakter yang dibaca adalah angka
            temp += int(read_file[i])
        elif read_file[i].isalpha(): # Jika karakter yang dibaca adalah huruf
            new_string += read_file[i]
        elif read_file[i] == " ": # Jika karakter yang dibaca adalah spasi
            new_string += read_file[i]

        if (read_file[i] == "\n") or (i == len(read_file) - 1):
            if temp % 2 == 0: # Jika angka - angka yang terdapat pada setiap baris berjumlah genap
                saved += 1
                print(new_string, file=file_output)
            new_string = "" 
            temp = 0 

    print()
    print("Total baris dalam file input: " + str(count_total_lines))
    print("Total baris yang disimpan: " + str(saved))

except FileNotFoundError: # Exception handling jika file yang dimasukkan tidak ditemukan
    print()
    print("Nama file yang anda masukkan tidak ditemukan")