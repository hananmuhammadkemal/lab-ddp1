def determinan_matriks():
    baris1 = input("baris 1 matriks: ")
    baris2 = input("baris 2 matriks: ")
    lst1 = list(int(baris1) for baris1 in baris1.split())
    lst2 = list(int(baris2) for baris2 in baris2.split())
    if len(lst1) == 2 and len(lst2) == 2:
        hasil = lst1[0] * lst2[1] - lst1[1] * lst2[0]
        print("Determinan matriks:", hasil)
        print()
        main()
    else:
        print("Terjadi kesalahan input. Silakan ulang kembali.")
        print()
        main()

def transpose_matriks():
    print("ukuran matriks: 2 x 2")
    print()
    baris1 = input("baris 1 matriks: ")
    baris2 = input("baris 2 matriks: ")
    lst1 = list(int(baris1) for baris1 in baris1.split())
    lst2 = list(int(baris2) for baris2 in baris2.split())
    if len(lst1) == 2 and len(lst2) == 2:
        hasil = str(lst1[0]) + " " + str(lst2[0]) + "\n" + str(lst1[1]) + " " + str(lst2[1])
        print("Matriks transpose:")
        print(hasil)
        print()
        main()
    else:
        print("Terjadi kesalahan input. Silakan ulang kembali.")
        print()
        main()
    
def penjumlahan_matriks():
    print("ukuran matriks 1: 2 x 2")
    baris1 = input("baris 1 matriks 1: ")
    baris2 = input("baris 2 matriks 1: ")
    print()
    print("ukuran matriks 2: 2 x 2")
    baris3 = input("baris 1 matriks 2: ")
    baris4 = input("baris 2 matriks 2: ")
    lst1 = list(int(baris1) for baris1 in baris1.split())
    lst2 = list(int(baris2) for baris2 in baris2.split())
    lst3 = list(int(baris3) for baris3 in baris3.split())
    lst4 = list(int(baris4) for baris4 in baris4.split())
    if len(lst1) == 2 and len(lst2) == 2 and len(lst3) == 2 and len(lst4) == 2:
        hasil = str(lst1[0] + lst3[0]) + " " + str(lst1[1] + lst3[1]) + "\n" + str(lst2[0] + lst4[0]) + " " + str(lst2[1] + lst4[1])
        print("Hasil penjumlahan matriks:")
        print(hasil)
        print()
        main()
    else:
        print()
        print("Terjadi kesalahan input. Silakan ulang kembali.")
        print()
        main()

def pengurangan_matriks():
    print("ukuran matriks 1: 2 x 2")
    baris1 = input("baris 1 matriks 1: ")
    baris2 = input("baris 2 matriks 1: ")
    print()
    print("ukuran matriks 2: 2 x 2")
    baris3 = input("baris 1 matriks 2: ")
    baris4 = input("baris 2 matriks 2: ")
    lst1 = list(int(baris1) for baris1 in baris1.split())
    lst2 = list(int(baris2) for baris2 in baris2.split())
    lst3 = list(int(baris3) for baris3 in baris3.split())
    lst4 = list(int(baris4) for baris4 in baris4.split())
    if len(lst1) == 2 and len(lst2) == 2 and len(lst3) == 2 and len(lst4) == 2:
        hasil = str(lst1[0] - lst3[0]) + " " + str(lst1[1] - lst3[1]) + "\n" + str(lst2[0] - lst4[0]) + " " + str(lst2[1] - lst4[1])
        print("Hasil pengurangan matriks:" + "\n" + hasil)
    else:
        print()
        print("Terjadi kesalahan input. Silakan ulang kembali.")
        print()
        main()

def perkalian_matriks():
    print("ukuran matriks 1: 2 x 2")
    baris1 = input("baris 1 matriks 1: ")
    baris2 = input("baris 2 matriks 1: ")
    print()
    print("ukuran matriks 2: 2 x 2")
    baris3 = input("baris 1 matriks 2: ")
    baris4 = input("baris 2 matriks 2: ")
    lst1 = list(int(baris1) for baris1 in baris1.split())
    lst2 = list(int(baris2) for baris2 in baris2.split())
    lst3 = list(int(baris3) for baris3 in baris3.split())
    lst4 = list(int(baris4) for baris4 in baris4.split())
    if len(lst1) == 2 and len(lst2) == 2 and len(lst3) == 2 and len(lst4) == 2:
        hasil = str(lst1[0] * lst3[0] + lst1[1] * lst4[0]) + " " + str(lst1[0] * lst3[1] + lst1[1] * lst4[1]) + "\n" + str(lst2[0] * lst3[0] + lst2[1] * lst4[0]) + " " + str(lst2[0] * lst3[1] + lst2[1] * lst4[1])
        print("Hasil perkalian matriks:" + "\n" + hasil)
    else:
        print()
        print("Terjadi kesalahan input. Silakan ulang kembali.")
        print()
        main()

def main():
    print("Selamat datang di Matrix Calculator. Berikut adalah operasi-operasi yang dapat dilakukan:")
    print("1. Penjumlahan")
    print("2. Pengurangan")
    print("3. Transpose")
    print("4. Determinan")
    print("5. Perkalian")
    print("6. Keluar")
    print()
    pilihan = input("Silahkan pilih operasi: ")
    if pilihan == "1":
        print()
        penjumlahan_matriks()
    elif pilihan == "2":
        print()
        pengurangan_matriks()
    elif pilihan == "5":
        print()
        perkalian_matriks()
    elif pilihan == "3":
        print()
        transpose_matriks()
    elif pilihan == "4":
        print()
        determinan_matriks()
    elif pilihan == "6":
        print()
        print("Sampai Jumpa!")
    else:
        print()
        print("Terjadi kesalahan input. Silakan ulang kembali.")
        main()

main()