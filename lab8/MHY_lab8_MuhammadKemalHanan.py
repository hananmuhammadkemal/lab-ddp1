class KueUlangTahun:
    def __init__(self, tipe, harga, tulisan, angka_lilin, topping):
        self.__tipe = tipe
        self.__harga = harga
        self.__tulisan = tulisan
        self.__angka_lilin = angka_lilin
        self.__topping = topping
    
    def get_tipe(self):
        # TODO: Implementasikan getter untuk tipe!
        return self.__tipe

    def get_harga(self):
        # TODO: Implementasikan getter untuk harga!
        return self.__harga
    
    def get_tulisan(self):
        # TODO: Implementasikan getter untuk tulisan!
        return self.__tulisan
    
    def get_angka_lilin(self):
        # TODO: Implementasikan getter untuk angka_lilin!
        return self.__angka_lilin
    
    def get_topping(self):
        # TODO: Implementasikan getter untuk topping!
        return self.__topping

class KueSponge(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, rasa, warna_frosting, harga = 2500):
        # TODO: Implementasikan constructor untuk class ini!
        super().__init__("Sponge", harga, tulisan, angka_lilin, topping)
        self.__rasa = rasa
        self.__warna_frosting = warna_frosting
        self.__harga = harga
    
    # TODO: Implementasikan getter untuk semua instance attribute tambahan di kelas ini!
    def get_rasa(self):
        return self.__rasa
    
    def get_warna_frosting(self):
        return self.__warna_frosting
    
    def get_harga(self):
        return self.__harga

class KueKeju(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, jenis_kue_keju, harga=3000):
        # TODO: Implementasikan constructor untuk class ini!
        super().__init__("Keju", harga, tulisan, angka_lilin, topping)
        self.__jenis_kue_keju = jenis_kue_keju
        self.__harga = harga

    # TODO: Implementasikan getter untuk semua instance attribute tambahan di kelas ini!
    def get_jenis_kue_keju(self):
        return self.__jenis_kue_keju

    def get_harga(self):
        return self.__harga

class KueBuah(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, jenis_kue_buah, harga=3500):
        # TODO: Implementasikan constructor untuk class ini!
        super().__init__("Buah", harga, tulisan, angka_lilin, topping)
        self.__jenis_kue_buah = jenis_kue_buah
        self.__harga = harga
    
    # TODO: Implementasikan getter untuk semua instance attribute tambahan di kelas ini!
    def get_jenis_kue_buah(self):
        return self.__jenis_kue_buah

    def get_harga(self):
        return self.__harga

# Fungsi ini harus diimplementasikan!
def buat_custom_bundle():
    # TODO: Implementasikan menu untuk membuat custom bundle!
    print('''
Jenis kue:
1. Kue Sponge
2. Kue Keju
3. Kue Buah
    ''')

    tipe_kue = input("Pilih tipe kue: ")
    
    if tipe_kue == "1":
        rasa = input("Rasa (Coklat/Stroberi): ")
        warna_frosting = input("Warna frosting (Coklat/Pink): ")
        topping = input("Topping (Ceri/Stroberi): ")
        jumlah_lilin = input("Masukkan angka lilin: ")
        tulisan = input("Masukkan tulisan: ")
        return KueSponge(tulisan, jumlah_lilin, topping, rasa, warna_frosting)

    elif tipe_kue == "2":
        jenis_kue_keju = input("Pilih jenis kue keju (New York/Japanese): ")
        topping = input("Pilih topping (Ceri/Stroberi): ")
        tulisan = input("Masukkan tulisan: ")
        jumlah_lilin = input("Masukkan angka lilin: ")
        return KueKeju(tulisan, jumlah_lilin, topping, jenis_kue_keju)

    elif tipe_kue == "3":
        jenis_kue_buah = input("Pilih jenis kue buah (American/British): ")
        topping = input("Pilih topping (Ceri/Stroberi): ")
        tulisan = input("Masukkan tulisan: ")
        jumlah_lilin = input("Masukkan angka lilin: ")
        return KueBuah(tulisan, jumlah_lilin, topping, jenis_kue_buah)

# Fungsi ini harus diimplementasikan!
def pilih_premade_bundle():
    # TODO: Implementasikan menu untuk memilih premade bundle!
    print('''
Pilihan paket istimewa:
1. New York-style Cheesecake with Strawberry Topping
2. Chocolate Sponge Cake with Cherry Topping and Blue Icing
3. American Fruitcake with Apple-Grape-Melon-mix Topping
''')

    paket_pilihan = input("Pilih paket: ")
    jumlah_lilin = input("Masukkan angka lilin: ")
    tulisan = input("Masukkan tulisan: ")

    if paket_pilihan == "1":
        return KueUlangTahun("New York-style Cheesecake", 2700, tulisan, jumlah_lilin, "Strawberry")

    elif paket_pilihan == "2":
        return KueUlangTahun("Sponge Cake", 2200, tulisan, jumlah_lilin, "Cherry and Blue Icing")

    elif paket_pilihan == "3":
        return KueUlangTahun("American Fruitcake", 3100, tulisan, jumlah_lilin, "Apple-Grape-Melon-mix")

    else:
        print("Paket tidak ditemukan!")

# Fungsi ini harus diimplementasikan!
def print_detail_kue(kue):
    # TODO: Implementasikan kode untuk print detail dari suatu kue!
    if kue.get_tipe() == "Buah":
        print("Kue " + kue.get_tipe() + " " + kue.get_jenis_kue_buah + " dengan topping " +  kue.get_topping() + "\nTulisan ucapan yang anda inginkan adalah " + '"' + kue.get_tulisan() + '"' + "\nAngka lilin yang anda pilih adalah " + kue.get_angka_lilin() + "\nHarga: " + kue.get_harga())
    elif kue.get_tipe() == "Keju":
        print("Kue " + kue.get_tipe() + " " + kue.get_jenis_kue_keju + " dengan topping " +  kue.get_topping() + "\nTulisan ucapan yang anda inginkan adalah " + '"' + kue.get_tulisan() + '"' + "\nAngka lilin yang anda pilih adalah " + str(kue.get_angka_lilin()) + "\nHarga: " + str(kue.get_harga()))
    elif kue.get_tipe() == "Sponge":
        print("Kue " + kue.get_tipe() + " " + kue.get_rasa + " " + kue.get_warna_frosting + " dengan topping " +  kue.get_topping() + "\nTulisan ucapan yang anda inginkan adalah " + '"' + kue.get_tulisan() + '"' + "\nAngka lilin yang anda pilih adalah " + str(kue.get_angka_lilin()) + "\nHarga: " + str(kue.get_harga()))
    else:
        print("Kue " + kue.get_tipe() + " dengan topping " +  kue.get_topping() + "\nTulisan ucapan yang anda inginkan adalah " + '"' + kue.get_tulisan() + '"' + "\nAngka lilin yang anda pilih adalah " + str(kue.get_angka_lilin()) + "\nHarga: " + str(kue.get_harga()))


# Fungsi main jangan diubah!
def main():
    print("Selamat datang di Homura!")
    print("Saat ini sedang diadakan event khusus bertema kue ulang tahun.")

    is_ganti = True

    while is_ganti:
        print("\nBundle kue yang kami sediakan: ")
        print("1. Bundle pre-made")
        print("2. Bundle custom\n")

        pilihan_bundle = input("Pilih bundle: ")

        kue = None

        while True:
            if pilihan_bundle == "1":
                kue = pilih_premade_bundle()
                break
            elif pilihan_bundle == "2":
                kue = buat_custom_bundle()
                break
            else:
                print("Pilihan anda tidak valid.")
                pilihan_bundle = input("Pilih paket: ")
        
        print("\nBerikut adalah kue pesanan anda: ")

        print_detail_kue(kue)

        while True:
            ganti = input("Apakah anda ingin mengganti pesanan anda? (Ya/Tidak) ")

            if ganti == "Ya":
                break
            elif ganti == "Tidak":
                is_ganti = False
                break
            else:
                print("Pilihan anda tidak valid.")
                ganti = input("Apakah anda ingin mengganti pesanan anda? (Ya/Tidak) ")
    
    print("\nTerima kasih sudah berbelanja di Homura!")

if __name__ == "__main__":
    main()