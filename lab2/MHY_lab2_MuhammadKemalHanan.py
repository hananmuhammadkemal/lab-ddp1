# Variabel  yang diperlukan
el_hp = 100
raiden_hp = 100
turn = 0
ulti = 1

# Loop Permainan 
while not(raiden_hp < 0 or el_hp < 0):
      turn += 1
      print(f"HP El: {el_hp}")
      print(f"HP Raiden: {raiden_hp}")
      print(f"Putaran ke-{turn}")
      print("==================")
      if turn % 3 == 0:       # Jika ronde merupakan kelipatan tiga
            print("Raiden akan menggunakan『The Final Calamity』")

      # Pilihan pemain
      print("Apa yang ingin anda lakukan?")
      print("1. Menyerang")
      print("2. Menghindar")
      if ulti == 1:           # Jika Ulti belum digunakan
            print("3. Menggunakan『Musou no Hitotachi』")
      action = int(input("Masukkan pilihan anda: "))

      # Jika ronde merupakan kelipatan tiga
      if turn % 3 == 0:
            if action == 1:
                  el_hp -= 50
                  raiden_hp -= 20
                  print("==================")
                  print("El menyerang dan mengurangi HP Raiden sebanyak 20")
                  print("Raiden menggunakan『The Final Calamity』dan mengurangi HP El sebanyak 50")
                  print("==================")
            elif action == 2:
                  print("==================")
                  print("Raiden menyerang namun El berhasil menghindar")
                  print("==================")
            elif action == 3:
                  if ulti == 1:
                        el_hp -= 50
                        raiden_hp -= 50
                        ulti -= 1
                        print("==================")
                        print("El menggunakan『Musou no Hitotachi』dan mengurangi HP Raiden sebanyak 50")
                        print("Raiden menggunakan『The Final Calamity』dan mengurangi HP El sebanyak 50")
                        print("==================")
                  else:             # Jika Ulti sudah digunakan
                        el_hp -= 50
                        print("==================")
                        print("El gagal melakukan apa - apa")
                        print("Raiden menggunakan『The Final Calamity』dan mengurangi HP El sebanyak 50")
                        print("==================")
            else:                   # Jika pilihan tidak valid
                  el_hp -= 50
                  print("==================")
                  print("El gagal melakukan apa - apa")
                  print("Aksi yang dipilih tidak valid. Silahkan coba lagi")
      
      # Jika ronde tidak merupakan kelipatan tiga
      else:
            if action == 1:
                  el_hp -= 20
                  raiden_hp -= 20
                  print("==================")
                  print("El menyerang dan mengurangi HP Raiden sebanyak 20")
                  print("Raiden menyerang dan mengurangi HP El sebanyak 20")
                  print("==================")
            elif action == 2:
                  print("==================")
                  print("Raiden menyerang namun El berhasil menghindar")
                  print("==================")
            elif action == 3:
                  if ulti == 1:
                        el_hp -= 20
                        raiden_hp -= 50
                        ulti -= 1
                        print("==================")
                        print("El menggunakan『Musou no Hitotachi』dan mengurangi HP Raiden sebanyak 50")
                        print("Raiden menyerang dan mengurangi HP El sebanyak 20")
                        print("==================")
                  else:       # Jika Ulti sudah digunakan
                        el_hp -= 20
                        print("==================")
                        print("El gagal melakukan apa - apa")
                        print("Raiden menyerang dan mengurangi HP El sebanyak 20")
                        print("==================")
            else:             # Jika memasukkan input yang tidak valid
                  el_hp -= 20
                  print("==================")
                  print("El gagal melakukan apa - apa")
                  print("Raiden menyerang dan mengurangi HP El sebanyak 20")
                  print("==================")
            
# Akhir permainan
if raiden_hp <= 0 and el_hp <= 0:
      print("Pertandingan Seri")
elif el_hp <= 0:
      print("Raiden menang")
else:
      print("El Menang")
