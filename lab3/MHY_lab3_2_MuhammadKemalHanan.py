input = input("Masukkan pesan rahasia: ")

start = input.find('*')
end = input.find('*', start+1) + 1

key = int(input[start+1:end-1])
n = 20220310 - key

remove_key = input[start:end]
input = input.replace(remove_key, "")

list_enkripsi = list(input)
pesan_asli = ""

for enkripsi in list_enkripsi:
    if enkripsi.isupper():
        lower = enkripsi.lower()
        ascii = ord(lower) - n
        while ascii < 97:
            ascii += 26
        pesan_asli += chr(ascii)
    elif enkripsi.isdigit():
        angka = int(enkripsi)
        angka = (n * 2) - ((n * 2) - angka)
        string_angka = str(angka)
        pesan_asli += string_angka
    elif enkripsi == '$':
        pesan_asli += ' '

print(pesan_asli)