import random

paragraf = "the quick brown fox jumped over the lazy dog's back. waltz, nymph, for quick jigs vex bud. sphinx of black quartz, judge my vow. pack my box with five dozen liquor jugs. glib jocks quiz nymph to vex dwarf. jackdaws love my big sphinx of quartz. the five boxing wizards jump quickly. how vexingly quick daft zebras jump! quick zephyrs blow, vexing daft jim. two driven jocks help fax my big quiz. the jay, pig, fox, zebra and my wolves quack! sympathizing would fix quaker objectives. a wizards job is to vex chumps quickly in fog. watch jeopardy!, alex trebek s fun tv quiz game. by jove, my quick study of lexicography won a prize! waxy and quivering, jocks fumble the pizza. the quick brown fox jumped over the lazy dog's back. waltz, nymph, for quick jigs vex bud. sphinx of black quartz, judge my vow. pack my box with five dozen liquor jugs. glib jocks quiz nymph to vex dwarf. jackdaws love my big sphinx of quartz. the five boxing wizards jump quickly. how vexingly quick daft zebras jump! quick zephyrs blow, vexing daft jim. two driven jocks help fax my big quiz. the jay, pig, fox, zebra and my wolves quack! sympathizing would fix quaker objectives. a wizards job is to vex chumps quickly in fog. watch jeopardy!, alex trebek s fun tv quiz game. by jove, my quick study of lexicography won a prize! waxy and quivering, jocks fumble the pizza. "
kunci = 20220310

pesan = input("Masukkan pesan yang akan dikirim: ")
pesan = pesan.lower()
list_pesan = list(pesan)
n = int(input("Pilih nilai n: "))

i = 0
for pesan in list_pesan:
    if pesan == " ":
        list_pesan[i] = "$"
    elif pesan.isdigit():
        number = int(list_pesan[i])
        number += (int(n) * 2)
        str_number = str(number)
        list_pesan[i] = str_number[-1]
    else:
        ascii = ord(list_pesan[i]) + int(n)
        while ascii > 122:
            ascii -= 26
        list_pesan[i] = chr(ascii)
    i += 1

kunci -= int(n)
format_kunci = "*" + str(kunci) + "*"
posisi = random.randint(0, len(paragraf))

f = 0
for pesan in list_pesan:
    if pesan == '$' or pesan.isdigit():
        f += 1
        paragraf = paragraf[:f]+str(pesan)+paragraf[f:]
    else:
        find = paragraf.find(pesan)
        while find < f:
            find = paragraf.find(pesan, find+1)
        paragraf = paragraf[:find]+pesan.upper()+paragraf[find:]
        f = find

paragraf = paragraf[:posisi]+\
    str(format_kunci)+paragraf[posisi:]

print(paragraf)
